#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

// Nombre del módulo y el archivo en /dev asociado a este
#define DEV_NAME "practica-2"

int entero = 0;
module_param(entero, int, 0);
MODULE_PARM_DESC(entero, "Número a imprimir");

char *cadena = "Hello";
module_param(cadena, charp, 0);
MODULE_PARM_DESC(cadena, "Cadena a imprimir");

int arreglo[3];
int sizeee;
module_param_array(arreglo, int, &sizeee, 0);
MODULE_PARM_DESC(arreglo, "Arreglo a imprimir");

/* device_open - informa al kernel que se abrió el archivo asociado al driver
 * y evita de esta forma que el modulo sea eliminado del kernel usando rmmod
 * mientras el archivo este abierto.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
static int device_open (struct inode *i, struct file *f) {
    printk(KERN_INFO DEV_NAME ": Dispositivo abierto");
    // Esta función informa al kernel que se esta usando el módulo, por cada una
    // de las llamadas a esta función debe haber una llamada a module_put
    try_module_get(THIS_MODULE);
    return 0;
}

/* device_release -  informa al kernel que el archivo asociado al driver ya no
 * esta en uso.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
static int device_release(struct inode *i, struct file *f) {
    printk(KERN_INFO DEV_NAME ": Dispositivo cerrado");
    module_put(THIS_MODULE);
    return 0;
}

// Mensaje que se copiará en el buffer del usuario
static char *msg = "Hola ";

/* device_read - Copia el mensaje a un buffer en espacio de usuario.
 * NOTA: La implementación de esta función en tu práctica debe cuidar que no se
 * copien más bytes de los indicados por el parámetro count. Copia la firma de 
 * la función (tipo de retorno y el tipo de parametros) para tú practica.
 */
static ssize_t device_read(struct file *file, char __user *buf, size_t count, loff_t *o) {
    int res = count;
    while (res > 6) {
        copy_to_user(buf, msg, 6);
        res -= 6;
    }
    return count - res;
}

/* device_write - Devuelve error al intentar abrir el archivo asociado a este 
 * modulo para escritura.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
ssize_t device_write(struct file *f, const char *c, size_t s, loff_t *o) {
    return -EINVAL;
}

// Operaciones soportadas por el archivo en /dev 
static struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = device_open,
    .release = device_release,
    .write = device_write,
    .read = device_read
};

int __init mod_init(void) {
    int i = 0;
    printk(KERN_INFO "%s mundo! entero: %d\n", cadena, entero);
    for (i = 0; i < sizeee; i++) {
        printk(KERN_INFO "%d\n", arreglo[i]);
    }
    return 0;
}

void __exit mod_cleanup(void) {
    printk(KERN_INFO "Adios mundo!\n");
    return;
}

module_init(mod_init);
module_exit(mod_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Equipo SO");
MODULE_DESCRIPTION("Practica 2");
